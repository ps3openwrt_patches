#!/bin/sh

append DRIVERS "wext"

scan_wext() {
	local device="$1"
	local adhoc sta ap monitor mesh disabled

	config_get vifs "$device" vifs

	for vif in $vifs; do
		config_get_bool disabled "$vif" disabled 0
		[ $disabled = 0 ] || continue

		config_get ifname "$vif" ifname
		config_set "$vif" ifname "${ifname:-$device}"

		config_get mode "$vif" mode

		case "$mode" in
			sta)
				append $mode "$vif"
			;;
			*)
				echo "$device($vif): Invalid mode, ignored."
				continue
			;;
		esac
	done

	config_set "$device" vifs "${ap:+$ap }${adhoc:+$adhoc }${sta:+$sta }${monitor:+$monitor }${mesh:+$mesh}"
}

disable_wext() {
	local device="$1"

	set_wifi_down "$device"

	for pid in `pidof wpa_supplicant`; do
		grep -E "$device" /proc/$pid/cmdline >/dev/null 2>/dev/null && \
			kill $pid
	done

	ifconfig "$device" down

	return 0
}

enable_wext() {
	local device="$1"
	local i=0

	config_get vifs "$device" vifs

	for vif in $vifs; do
		config_get ifname "$vif" ifname
		config_get mode "$vif" mode

		config_set "$vif" ifname "$ifname"

		ifconfig "$ifname" up
		set_wifi_up "$vif" "$ifname"

		case "$mode" in
			sta)
				if eval "type wpa_supplicant_setup_vif" 2>/dev/null >/dev/null; then
					wpa_supplicant_setup_vif "$vif" wext || {
						echo "enable_wext($device): Failed to set up wpa_supplicant for interface $ifname" >&2
						ifconfig "$ifname" down
						continue
					}
				fi
			;;
		esac

		i=$(($i + 1))
	done
}
